##
## Makefile for CPE_2014_lemin in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin
## 
## Made by sebastien le-maire
## Login   <le-mai_s@epitech.net>
## 
## Started on  Sat Sep 12 10:32:15 2015 sebastien le-maire
## Last update dim. sept. 20 23:33:35 2015 Franck Hochstaetter
##

ifeq ($(DEBUG), yes)

CFLAGS		+= -g -g3 -ggdb

endif

PATH_LIB	= ./lib/
PATH_SRCS	= ./sources/

NAME		= lem_in
LIB_NAME	= $(PATH_LIB)libmyc.a

AR		= ar rc
RAN		= ranlib
CC		= gcc
RM		= rm -f

CFLAGS		+= -Wall -Wextra
CFLAGS		+= -Werror
CFLAGS		+= -I./include

LDFLAGS		+= -L$(PATH_LIB) -lmyc

SRCS		+= $(PATH_SRCS)comment.c
SRCS		+= $(PATH_SRCS)link.c
SRCS		+= $(PATH_SRCS)matrix.c
SRCS		+= $(PATH_SRCS)main.c
SRCS		+= $(PATH_SRCS)nb_ants.c
SRCS		+= $(PATH_SRCS)node.c
SRCS		+= $(PATH_SRCS)parse.c
SRCS		+= $(PATH_SRCS)parse_is.c
SRCS		+= $(PATH_SRCS)path_finding.c
SRCS		+= $(PATH_SRCS)path_list.c
SRCS		+= $(PATH_SRCS)print_way.c
SRCS		+= $(PATH_SRCS)node_path.c
SRCS		+= $(PATH_SRCS)find_the_way.c
SRCS		+= $(PATH_SRCS)check_start_and_end_room.c
SRCS_LIB	+= $(PATH_LIB)fail.c
SRCS_LIB	+= $(PATH_LIB)my_putchar.c
SRCS_LIB	+= $(PATH_LIB)my_putstr.c
SRCS_LIB	+= $(PATH_LIB)my_strlen.c
SRCS_LIB	+= $(PATH_LIB)my_puterror.c
SRCS_LIB	+= $(PATH_LIB)my_put_err.c
SRCS_LIB	+= $(PATH_LIB)my_putnbr.c
SRCS_LIB	+= $(PATH_LIB)my_putnbr_base.c
SRCS_LIB	+= $(PATH_LIB)my_atoi.c
SRCS_LIB	+= $(PATH_LIB)my_getnbr.c
SRCS_LIB	+= $(PATH_LIB)my_strcpy.c
SRCS_LIB	+= $(PATH_LIB)my_strncpy.c
SRCS_LIB	+= $(PATH_LIB)my_strcat.c
SRCS_LIB	+= $(PATH_LIB)my_strncat.c
SRCS_LIB	+= $(PATH_LIB)my_strcmp.c
SRCS_LIB	+= $(PATH_LIB)my_strncmp.c
SRCS_LIB	+= $(PATH_LIB)my_strdup.c
SRCS_LIB	+= $(PATH_LIB)my_strndup.c
SRCS_LIB	+= $(PATH_LIB)newstr.c
SRCS_LIB	+= $(PATH_LIB)my_str_to_wordtab.c
SRCS_LIB	+= $(PATH_LIB)show_strtab.c
SRCS_LIB	+= $(PATH_LIB)destroy_strtab.c
SRCS_LIB	+= $(PATH_LIB)my_memset.c
SRCS_LIB	+= $(PATH_LIB)get_next_line.c

OBJS		= $(SRCS:.c=.o)
OBJS_LIB	= $(SRCS_LIB:.c=.o)

all: $(LIB_NAME) $(NAME)

$(LIB_NAME): $(OBJS_LIB)
	$(AR) $(LIB_NAME) $(OBJS_LIB)
	$(RAN) $(LIB_NAME)

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

clean:
	$(RM) $(OBJS)
	$(RM) $(OBJS_LIB)

fclean: clean
	$(RM) $(NAME)
	$(RM) $(LIB_NAME)

re: fclean all

.PHONY: all clean fclean re
