/*
** print_way.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
** 
** Made by sami farhane
** Login   <farhan_s@epitech.net>
** 
** Started on  Sat Sep 12 19:56:59 2015 sami farhane
** Last update dim. sept. 20 22:54:47 2015 Franck Hochstaetter
*/

#include <unistd.h>
#include "lem_in.h"
#include "myc.h"

void	the_way(int *anti, t_path **tmp, t_ctrl_path *path, t_graph *graf)
{
  my_putchar('P');
  my_putnbr(*anti);
  my_putchar('-');
  my_putstr((*tmp)->name);
  *tmp = (*tmp)->prev;
  ++(*anti);
  if (*tmp != path->first && graf->nb_ants >= *anti)
    my_putchar(' ');
}

void		print_way(t_graph *graf, t_ctrl_path *path)
{
  int		ant;
  int		anti;
  int		end;
  t_path	*tmp;
  t_path	*head;

  ant = 1;
  end = 0;
  head = path->first->next;
  while (graf->nb_ants > end)
    {
      tmp = head;
      anti = ant;
      while (tmp != path->first && graf->nb_ants >= anti && tmp != NULL)
	the_way(&anti, &tmp, path, graf);
      if (head == path->last)
	{
	  end = end + 1;
	  ant = ant + 1;
	}
      else
	head = head->next;
      my_putchar('\n');
    }
}
