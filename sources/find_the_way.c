/*
** find_the_way.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
** 
** Made by sami farhane
** Login   <farhan_s@epitech.net>
** 
** Started on  Tue Sep 15 18:30:16 2015 sami farhane
** Last update dim. sept. 20 22:56:59 2015 Franck Hochstaetter
*/

#include "lem_in.h"
#include "myc.h"

static int	check_nb_link(t_graph *graf, int i)
{
  int		j;
  int		links;

  j = 0;
  links = 0;
  while (j < graf->nb_node)
    {
      if (graf->matrix[i][j] == 1)
	links = links + 1;
      j = j + 1;
    }
  return (links);
}

static void	add_room_to_the_way(t_graph *graf, t_ctrl_path *path, int j)
{
  t_node	*tmp;

  tmp = graf->node_list;
  while (tmp->id != j)
    tmp = tmp->next;
  path_list(path, tmp->name);
}

int		find_the_way(t_graph *graf, t_ctrl_path *path)
{
  int		i;
  int		j;

  i = graf->start->id;
  j = 0;
  path_list(path, graf->start->name);
  while (graf->matrix[i][graf->end->id] != 1 && j < graf->nb_node)
    {
      if (graf->matrix[i][j] == 1)
	{
	  i = j;
	  add_room_to_the_way(graf, path, i);
	}
      j = j + 1;
    }
  if (graf->matrix[i][graf->end->id] == 1)
    path_list(path, graf->end->name);
  return (0);
}

int		check_the_way(t_graph *graf, t_ctrl_path *path)
{
  int		tmp;
  int		i;
  int		j;

  i = graf->start->id;
  j = -1;
  while (graf->matrix[i][graf->end->id] != 1 && ++j < graf->nb_node - 1)
    if (graf->matrix[i][j] == 1)
      {
	if (check_nb_link(graf, i) > 1)
	  i = j;
	if (check_nb_link(graf, i) == 1)
	  {
	    tmp = i;
	    i = j;
	    j = tmp;
	    graf->matrix[i][j] = 0;
	    j = 0;
	  }
	if (check_nb_link(graf, i) == 0)
	  fail(MSG_ERR_PATH, 21);
      }
  find_the_way(graf, path);
  return (0);
}
