/*
** link.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
** 
** Made by Franck Hochstaetter
** Login   <hochst_a@epitech.net>
** 
** Started on  mer. sept. 16 16:09:35 2015 Franck Hochstaetter
** Last update dim. sept. 20 22:55:49 2015 Franck Hochstaetter
*/

#include <stdlib.h>
#include "myc.h"
#include "lem_in.h"

char		*lem_link_get_name_b(char *line)
{
  char		*name;
  int		len;
  int		i;

  len = 0;
  i = 0;
  while (line[len] && line[len] != ' ' && line[len] != '\t' && line[len] != '#')
    len++;
  if ((name = malloc(sizeof(char) * len + 1)) == NULL)
    fail(MSG_ERR_MALLOC, 5);
  while (line[i] && line[i] != ' ' && line[i] != '\t' && line[i] != '#'
	 && i < len)
    {
      name[i] = line[i];
      i++;
    }
  name[i] = '\0';
  return (name);
}

char		*lem_link_get_name_a(char *line, int *i)
{
  char		*name;
  int		len;

  *i = 0;
  len = 0;
  while (line[len] && line[len] != '-')
    {
      if (line[len] == '#')
	exit(-1);
      len++;
    }
  if ((name = malloc(sizeof(char) * len + 1)) == NULL)
    fail(MSG_ERR_MALLOC, 5);
  while (line[*i] && line[*i] != '-' && line[*i] != '#' && *i < len)
    {
      name[*i] = line[*i];
      (*i)++;
    }
  name[*i] = '\0';
  return (name);
}

int		lem_link_get_node_id(t_node *node_list, char *name)
{
  while (node_list && name)
    {
      if (!my_strcmp(node_list->name, name))
	return (node_list->id);
      node_list = node_list->next;
    }
  return (-1);
}

void		lem_link_insert(t_graph *graph, char *line)
{
  char		*a;
  char		*b;
  int		i;
  int		x;
  int		y;

  if (graph->matrix == NULL)
    graph->matrix = (int **)init_matrix(graph->nb_node);
  a = lem_link_get_name_a(line, &i);
  i += (line[i] == '-' ? 1 : 0);
  b = lem_link_get_name_b(line + i);
  x = lem_link_get_node_id(graph->node_list, a);
  y = lem_link_get_node_id(graph->node_list, b);
  if (x != y && x != -1 && y != -1)
    {
      graph->matrix[x][y] = 1;
      graph->matrix[y][x] = 1;
    }
  (graph->nb_link)++;
}
