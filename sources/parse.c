/*
** parse.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Sep 14 18:07:31 2015 sebastien le-maire
** Last update dim. sept. 20 22:45:35 2015 Franck Hochstaetter
*/

#include <stdlib.h>
#include "myc.h"
#include "lem_in.h"

void	lem_parse_get_pos(const char *line, t_pos *next_pos)
{
  if (!(my_strncmp(line, "##start", 8)))
    *next_pos = START;
  else if (!(my_strncmp(line, "##end", 6)))
    *next_pos = END;
  else
    *next_pos = NODE;
}

int	lem_parse_next_line(char **line)
{
  if (*line != NULL)
    free(*line);
  *line = NULL;
  if (!(*line = get_next_line(0)))
    return (EXIT_FAILURE);
  return (EXIT_SUCCESS);
}

int	lem_parse(t_graph *graph)
{
  char	*line;
  t_pos	next_pos;

  next_pos = 0;
  line = NULL;
  if ((graph->nb_ants = get_nb_ants()) == 0)
    exit(-1);
  while (!lem_parse_next_line(&line))
    {
      if (next_pos == NUL)
	lem_parse_get_pos(line, &next_pos);
      if (line[0] != '#' && lem_parse_is_name_line(line) && !graph->matrix)
	lem_node_insert(graph, line, &next_pos);
      else if (line[0] != '#' && lem_parse_is_link_line(line))
	lem_link_insert(graph, line);
      else if (!line[0] || line[0] != '#')
	fail("Syntax Error", 15);
      my_putstr(line);
      my_putchar('\n');
    }
  if (graph->start == NULL)
    fail(MSG_ERR_START_NOT_FOUND, -1);
  if (graph->end == NULL)
    fail(MSG_ERR_END_NOT_FOUND, -1);
  return (0);
}
