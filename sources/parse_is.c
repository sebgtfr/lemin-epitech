/*
** parse_is.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
** 
** Made by Franck Hochstaetter
** Login   <hochst_a@epitech.net>
** 
** Started on  mar. sept. 15 17:45:00 2015 Franck Hochstaetter
** Last update dim. sept. 20 22:45:24 2015 Franck Hochstaetter
*/

#include <stdlib.h>

int	lem_parse_is_name_line(char *line)
{
  int	i;

  i = 0;
  while (line[i] != ' ' && line[i] != '\t')
    {
      if (line[i] == '-' || line[i] == '#')
	return (0);
      i++;
      if (!line[i])
	return (0);
    }
  i++;
  while (line[i] >= '0' && line[i] <= '9')
    i++;
  i++;
  while (line[i] >= '0' && line[i] <= '9')
    i++;
  while (line[i] && (line[i] == ' ' || line[i] == '\t' || line[i] == '#'))
    if (line[i++] == '#')
      return (1);
  return (!line[i]  ? 1 : 0);
}

int	lem_parse_is_link_line(char *line)
{
  int	i;

  i = 0;
  while (line[i] && line[i] != '-')
    {
      if (line[i] == '#')
	return (0);
      i++;
      if (!line[i])
	return (0);
    }
  i++;
  while (line[i] && line[i] != ' ' && line[i] != '\t')
    {
      if (line[i] == '#')
	return (1);
      if (line[i] == '-')
	return (0);
      i++;
    }
  while (line[i] && (line[i] == ' ' || line[i] == '\t' || line[i] == '#'))
    if (line[i++] == '#')
      return (1);
  return (!line[i] ? 1 : 0);
}
