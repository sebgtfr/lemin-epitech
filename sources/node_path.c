/*
** node_path.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
** 
** Made by sami farhane
** Login   <farhan_s@epitech.net>
** 
** Started on  Mon Sep 14 17:47:11 2015 sami farhane
** Last update dim. sept. 20 22:45:58 2015 Franck Hochstaetter
*/

#include <stdlib.h>
#include "lem_in.h"

char		*node_path(t_ctrl_path *path)
{
  t_path	*node_act;
  char		*name_node;

  if (path == NULL)
    return (NULL);
  if (path->first->next != NULL)
    {
      node_act = path->first->next;
      name_node = node_act->name;
      path->first->next = node_act->next;
      free(node_act);
    }
  return (name_node);
}
