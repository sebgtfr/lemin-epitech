/*
** path_list.c for  in /home/farhan_s/rendu/prog_elem/CPE_2014_lemin/solobaby/sources
** 
** Made by sami farhane
** Login   <farhan_s@epitech.net>
** 
** Started on  Sat Sep 12 17:49:08 2015 sami farhane
** Last update Sun Sep 20 17:27:40 2015 sami farhane
*/

#include <unistd.h>
#include <stdlib.h>
#include "lem_in.h"
#include "myc.h"

int		path_list(t_ctrl_path *path, char *node)
{
  t_path	*new;
  t_path	*elem_act;

  if (path == NULL || (new = malloc(sizeof(t_path))) == NULL)
    return (-1);
  new->name = node;
  new->next = NULL;
  if (path->first != NULL)
    {
      elem_act = path->first;
      while (elem_act->next != NULL)
	elem_act = elem_act->next;
      new->prev = elem_act;
      elem_act->next = new;
      path->last = new;
    }
  else
    {
      path->first = new;
      path->last = new;
      path->first->prev = NULL;
    }
  return (0);
}
