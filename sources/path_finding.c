/*
** path_finding.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
** 
** Made by sami farhane
** Login   <farhan_s@epitech.net>
** 
** Started on  Sat Sep 12 17:34:40 2015 sami farhane
** Last update Sun Sep 20 21:13:35 2015 sami farhane
*/

#include <stdlib.h>
#include "myc.h"
#include "lem_in.h"

int		path_finding(t_graph *graf)
{
  t_ctrl_path	*path;

  if ((path = malloc(sizeof(t_ctrl_path))) == NULL)
    {
      my_puterror(MSG_ERR_MALLOC);
      return (-1);
    }
  path->first = NULL;
  path->last = NULL;
  if ((check_start_and_end_room(graf)) == -1)
    return (-1);
  if ((check_the_way(graf, path)) == -1)
    return (-1);
  print_way(graf, path);
  return (0);
}
