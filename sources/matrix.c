/*
** matrix.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Sep 14 17:39:16 2015 sebastien le-maire
** Last update dim. sept. 20 22:46:24 2015 Franck Hochstaetter
*/

#include <stdlib.h>
#include "lem_in.h"

unsigned int	**init_matrix(unsigned int size)
{
  unsigned int	**matrix;
  unsigned int	y;
  unsigned int	x;

  y = 0;
  if (!(matrix = malloc(sizeof(unsigned int *) * size)))
    return (NULL);
  while (y < size)
    {
      x = 0;
      if (!(matrix[y] = malloc(sizeof(unsigned int) * size)))
	return (NULL);
      while (x < size)
	{
	  matrix[y][x] = 0;
	  ++x;
	}
      ++y;
    }
  return (matrix);
}

void		delete_matrix(unsigned int **matrix, unsigned int size)
{
  unsigned int	y;

  y = 0;
  while (y < size)
    {
      free(matrix[y]);
      ++y;
    }
  free(matrix);
}
