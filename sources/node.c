/*
** node.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
** 
** Made by Franck Hochstaetter
** Login   <hochst_a@epitech.net>
** 
** Started on  mer. sept. 16 12:00:46 2015 Franck Hochstaetter
** Last update dim. sept. 20 23:00:41 2015 Franck Hochstaetter
*/

#include <stdlib.h>
#include "lem_in.h"
#include "myc.h"

int		lem_node_get_name_size(char *line)
{
  int		i;

  i = 0;
  while (line[i] && line[i] != ' ' && line[i] != '\t')
    {
      if (line[i] == '#')
	fail(MSG_ERR_NAME_INVALID, 17);
      i++;
    }
  return (i);
}

void		lem_node_get_name(char *line, t_node *node)
{
  int		i;
  int		len;

  len = lem_node_get_name_size(line);
  if ((node->name = malloc(sizeof(char) * len + 1)) == NULL)
    fail(MSG_ERR_MALLOC, 5);
  i = 0;
  while (line[i] && line[i] != ' ' && line[i] != '\t' && i < len)
    {
      node->name[i] = line[i];
      i++;
    }
  node->name[i] = 0;
}

void		lem_node_start_end(t_graph *graph, t_pos next_pos, t_node *node)
{
  if (next_pos == START)
    {
      if (graph->start != NULL)
	fail(MSG_ERR_START_TWICE, 11);
      else
	graph->start = node;
    }
  else if (next_pos == END)
    {
      if (graph->end != NULL)
	fail(MSG_ERR_END_TWICE, 12);
      else
	graph->end = node;
    }
}

void		lem_node_insert(t_graph *graph, char *line, t_pos *next_pos)
{
  t_node	*node;
  t_node	*tmp;

  if ((node = malloc(sizeof(t_node))) == NULL)
    fail(MSG_ERR_MALLOC, 5);
  tmp = graph->node_list;
  node->id = graph->nb_node;
  graph->node_list = (!graph->node_list ? node : graph->node_list);
  lem_node_start_end(graph, *next_pos, node);
  lem_node_get_name(line, node);
  node->next = NULL;
  (graph->nb_node)++;
  *next_pos = NUL;
  while (tmp && tmp->next)
    tmp = tmp->next;
  if (tmp)
    tmp->next = node;
  else
    tmp = node;
}
