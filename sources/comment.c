/*
** comment.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Tue Sep 15 09:53:22 2015 sebastien le-maire
** Last update dim. sept. 20 22:55:15 2015 Franck Hochstaetter
*/

#include <stdlib.h>
#include "myc.h"
#include "lem_in.h"

int		lem_comment(const char *str)
{
  unsigned int	i;

  i = 0;
  while (str[i] && (str[i] == ' ' || str[i] == '\t'))
    ++i;
  if (str[i] && str[i] != '#')
    my_puterror(MSG_ERR_COMMENT_INVALID);
  else
    return (EXIT_SUCCESS);
  return (EXIT_FAILURE);
}
