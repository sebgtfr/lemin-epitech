/*
** main.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Thu Aug 13 15:55:40 2015 sebastien le-maire
** Last update dim. sept. 20 22:17:58 2015 Franck Hochstaetter
*/

#include <stdlib.h>
#include "myc.h"
#include "lem_in.h"

void		lem_init_graph(t_graph *graph)
{
  graph->nb_node = 0;
  graph->nb_link = 0;
  graph->nb_ants = 0;
  graph->node_list = NULL;
  graph->start = NULL;
  graph->end = NULL;
  graph->matrix = NULL;
}

int		main(void)
{
  t_graph	graph;

  lem_init_graph(&graph);
  if (lem_parse(&graph))
    return (EXIT_FAILURE);
  if (graph.nb_link == 0 || graph.nb_node == 0)
    fail("No node or link found", 33);
  if (path_finding(&graph) == -1)
    return (EXIT_FAILURE);
  return (EXIT_SUCCESS);
}
