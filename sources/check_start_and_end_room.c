/*
** check_start_and_end_room.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
** 
** Made by sami farhane
** Login   <farhan_s@epitech.net>
** 
** Started on  Thu Sep 17 16:33:26 2015 sami farhane
** Last update dim. sept. 20 22:58:55 2015 Franck Hochstaetter
*/

#include "myc.h"
#include "lem_in.h"

int		check_start_and_end_room(t_graph *graf)
{
  int		j;

  j = 0;
  while (graf->matrix[graf->start->id][j] != 1 && j <= graf->nb_node)
    j = j + 1;
  if (graf->matrix[graf->start->id][j] != 1)
    {
      my_puterror(MSG_ERR_START_LINK);
      return (-1);
    }
  j = 0;
  while (graf->matrix[graf->end->id][j] != 1 && j <= graf->nb_node)
    j = j + 1;
  if (graf->matrix[graf->end->id][j] != 1)
    {
      my_puterror(MSG_ERR_END_LINK);
      return (-1);
    }
  return (0);
}
