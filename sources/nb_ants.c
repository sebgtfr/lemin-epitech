/*
** nb_ants.c for sources in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/sources
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Tue Sep 15 09:51:35 2015 sebastien le-maire
** Last update dim. sept. 20 22:46:12 2015 Franck Hochstaetter
*/

#include <stdlib.h>
#include "myc.h"
#include "lem_in.h"

int		line_ants(const char *line)
{
  unsigned int	i;

  i = 0;
  while (line[i] && line[i] >= '0' && line[i] <= '9')
    ++i;
  if (!i)
    return ((lem_comment(line)) ? -2 : -1);
  else
    return ((lem_comment(line + i)) ? -2 : my_atoi(line));
}

unsigned int	get_nb_ants(void)
{
  char		*line;
  int		nb_ants;

  while ((line = get_next_line(0)) && ((nb_ants = line_ants(line)) == -1))
    free(line);
  if (!line)
    my_puterror(MSG_ERR_NB_ANTS_NOT_FOUND);
  else if (!nb_ants)
    my_puterror(MSG_ERR_NB_ANTS_INVALID);
  else if (nb_ants > 0)
    {
      my_putstr(line);
      my_putstr("\n");
      free(line);
      return ((unsigned int)nb_ants);
    }
  free(line);
  return (0);
}
