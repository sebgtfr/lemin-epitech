### FONCTIONNEMENT ###

1) Map

To create map, you need create new file like this :

# = comment on file

[ants_counting] # first line, this is an unsigned integer
# Node sections
[node-name] [coordinate x] [coordinate y]
...
# You need to add two keywords on this section :
##start # where the ants are on beginning
[node-name] [coordinate x] [coordinate y]
##end # where the ants should go at the end.
[node-name] [coordinate x] [coordinate y]
# Connection betweens nodes section
[node1]-[node2] # create corridor between both nodes.

Path finding binary :

	    => ./lem_in < [map_for_pathfinding]

this write on console all file's contents use to generate path and
the path of all ants with this format :

P[ID_ants]-[node-name] ...
...

You can have several ants to move in same time.

Visual Binary :

- It take all return of path finding binary

       -> Using with file containing result of pathfinding :

       	  => ./visu_hex < [map_for_visu]

	-> Using with path finding binary :

	   => ./lem_in < [map_for_pathfinding] | ./visu_hex