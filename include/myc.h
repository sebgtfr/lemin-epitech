/*
** myc.h for include in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/include
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Thu Aug 13 08:56:46 2015 sebastien le-maire
** Last update dim. sept. 20 15:27:36 2015 Franck Hochstaetter
*/

#ifndef MYC_H_
# define MYC_H_

# define INT_TO_CHAR(nb) ((nb) + '0')
# define CHAR_TO_INT(nb) ((nb) - '0')
# define IS_NUM(nb) ((nb) >= '0' && (nb) <= '9')
# define NOT_NUM(nb) ((nb) < '0' || (nb) > '9')

typedef	enum	e_bool
  {
		TRUE,
		FALSE
  }		t_bool;

/*
**  ##########   Data of Get_next_line   ##########
*/
# define BUFF_SIZE (100)

typedef	struct	s_gnl
{
  char		buf[BUFF_SIZE + 1];
  char		*tmp;
  char		*line;
}		t_gnl;

/*
** ##########   Display functions   ##########
*/
void		my_putchar(const char c);
void		my_putstr(const char *str);
void		my_puterror(const char *str);
void		my_putnbr(int nb);
void		my_putnbr_base(int nbr, const char *base);
void		show_strtab(const char **tab, const char *separator);

/*
** ##########   Gets data functions   ##########
*/
unsigned int	my_strlen(const char *str);
int		my_atoi(const char *str);
int		my_getnbr(const char *str, t_bool count_sign);

/*
** ##########   String manipulate functions   ##########
*/
char		*my_strcpy(char *dest, const char *src);
char		*my_strncpy(char *dest, const char *src,
			    const unsigned int size);
char		*my_strcat(char *dest, const char *src);
char		*my_strncat(char *dest, const char *src,
			    const unsigned int size);
int		my_strcmp(const char *s1, const char *s2);
int		my_strncmp(const char *s1, const char *s2,
			   const unsigned int size);
char		*my_strdup(const char *src);
char		*my_strndup(const char *src, const unsigned int size);
char		*my_strdupcat(char *s1, const char *s2);
char		*newstr(const unsigned int size);
char		**my_str_to_wordtab(const char *str, const char *delimit);

/*
** #########    annex functions of my_str_to_wordtab   ##########
*/
unsigned int	mstwt_char_str_cmp(const char c, const char *delimit);
unsigned int	mstwt_count_word(const char *str, const char *delimit);
unsigned int	mstwt_count_char(const char *str, const char *delimit,
				 unsigned int pos);

/*
** ##########   memory manipulate functions   ##########
*/
void		*my_memset(void *mem, const unsigned int size);
void		destroy_strtab(char **tab);

/*
** #########    File Descriptor functions   ##########
*/
char		*get_next_line(const int fd);

/*
** #########    annex functions of get_next_line   ##########
*/
void		gnl_free_save(char **save);
int		gnl_prep_line(t_gnl *p, char **save, int ret);
int		gnl_transfer_save(t_gnl *p, char **save);
char		*gnl_realloc(char *s1, const char *s2);

/*
** DasFunc
*/
void		fail(char *mesa, int err);
int		my_put_err(char *str);

#endif /* !MYC_H_ */
