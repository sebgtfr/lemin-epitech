/*
** lem_in.h for include in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/include
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Thu Aug 13 15:51:01 2015 sebastien le-maire
** Last update dim. sept. 20 22:19:24 2015 Franck Hochstaetter
*/

#ifndef LEM_IN_H_
# define LEM_IN_H_

# define MSG_ERR_NB_ANTS_NOT_FOUND	"SyntaxError: Ants number not found"
# define MSG_ERR_NB_ANTS_INVALID	"SyntaxError: Ants number must be > 0"
# define MSG_ERR_COMMENT_INVALID	"SyntaxError: Invalid comment"
# define MSG_ERR_NAME_INVALID		"SyntaxError: Invalid name"
# define MSG_ERR_START_NOT_FOUND	"SyntaxError: Start not found"
# define MSG_ERR_END_NOT_FOUND		"SyntaxError: End not found"
# define MSG_ERR_START_TWICE		"SyntaxError: Start is declared twice"
# define MSG_ERR_END_TWICE		"SyntaxError: End is declared twice"
# define MSG_ERR_MALLOC			"Malloc Failed"
# define MSG_ERR_START_LINK		"GraphError: Start is not linked\n"
# define MSG_ERR_END_LINK		"GraphError: End is not linked\n"
# define MSG_ERR_PATH			"GraphError: Path not found"

typedef	enum	e_pos
{
		NUL,
		NODE,
		START,
		END,
}		t_pos;

typedef	struct	s_node
{
  char		*name;
  int		id;
  struct s_node	*next;
}		t_node;

typedef	struct	t_graph
{
  int		nb_ants;
  int		nb_node;
  int		nb_link;
  t_node	*node_list;
  t_node	*start;
  t_node	*end;
  int		**matrix;
}		t_graph;

typedef struct	s_path
{
  char		*name;
  struct s_path	*prev;
  struct s_path	*next;
}		t_path;

typedef struct	s_ctrl_path
{
  t_path	*first;
  t_path	*last;
}		t_ctrl_path;

/*
** comment.c
*/
int		lem_comment(const char *str);

/*
** link.c
*/
char		*lem_link_get_name_a(char *line, int *i);
char		*lem_link_get_name_b(char *line);
int		lem_link_get_node_id(t_node *node_list, char *name);
void		lem_link_insert(t_graph *graph, char *line);

/*
** list.c
*/
int		add_new_elem(t_node **p, const char *name, int id);
void		delete_graph(t_node **node);

/*
** main.c
*/
void		lem_init_graph(t_graph *graph);

/*
** matrix.c
*/
unsigned int	**init_matrix(unsigned int size);
void		delete_matrix(unsigned int **matrix, unsigned int size);

/*
** nb_ants.c
*/
int		line_ants(const char *line);
unsigned int	get_nb_ants(void);

/*
** node.c
*/
void		lem_node_insert(t_graph *graph, char *line, t_pos *next_pos);

/*
** parse.c
*/
int		lem_parse(t_graph *graph);

/*
** parse_is.c
*/
int		lem_parse_is_name_line(char *line);
int		lem_parse_is_link_line(char *line);

/*
** Path_finding
*/
int		path_finding(t_graph *graf);
int		path_list(t_ctrl_path *path, char *node);
void		print_way(t_graph *graf, t_ctrl_path *path);
char		*node_path(t_ctrl_path *path);
int		find_the_way(t_graph *graf, t_ctrl_path *path);
int		check_start_and_end_room(t_graph *graf);
int		check_the_way(t_graph *graf, t_ctrl_path *path);

#endif /* !LEM_IN_H_ */
