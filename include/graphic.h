/*
** graphic.h for include in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/include
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Tue Sep 15 11:36:23 2015 sebastien le-maire
** Last update dim. sept. 20 19:06:56 2015 Franck Hochstaetter
*/

#ifndef GRAPHIC_H_
# define GRAPHIC_H_

# include <SDL/SDL.h>
# include <GL/gl.h>
# include <GL/glu.h>
# include "parse_graphic.h"

# define WIN_X	(1200)
# define WIN_Y	(900)
# define MOVE	(0.5)

/*
** init_sdl.c
*/
int		init_sdl(void);

/*
** event.c
*/
void		fct_keypress(int keycode, t_cam *cam);
void		fct_keyrelease(int keycode, t_cam *cam);
void		fct_opengl(t_cam *cam, t_ogl_graph *graph);
void		timeout(t_cam *cam, t_ogl_graph *graph);
void		event_sdl(t_cam *cam, t_ogl_graph *graph);

/*
** cam.c
*/
void	        cam_move(t_cam *cam);
int		init_cam(t_cam *cam, t_top *start);

/*
** ants.c
*/
void		ants(GLUquadric *quadric, double x, double y, double z);
void		leg_left(GLUquadric *quadric, int a);
void		leg_left_back(GLUquadric *quadric, int a);
void		leg_right(GLUquadric *quadric, int a);
void		leg_right_back(GLUquadric *quadric, int a);

/*
** ants_body.c
*/
void		ants_body(GLUquadric *quadric);
void		ants_antenna(GLUquadric *quadric);

/*
** anthill.c
*/
void		anthill(GLUquadric *quadric, t_top *top,
			t_top *start, t_top *end);
void		place_sphere(GLUquadric *quadric, const double x,
			     const double y, const double z);

/*
** move_ants.c
*/
void		calc_forward(double (*forward)[3], double (*vector)[3]);
void		calc_vector(double (*vector)[3], double (*now)[3],
			    double (*after)[3]);
void		change_path(double (*vector)[3], t_ants *ants,
			    unsigned int *turn);
void		forward_ants(t_ants *ants, unsigned int *turn);
void		move_ants(GLUquadric *quadric, t_ogl_graph *graph);

#endif /* !GRAPHIC_H_ */
