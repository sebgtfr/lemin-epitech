/*
** lem_struct.h for include in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/include
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Tue Sep 15 14:55:33 2015 sebastien le-maire
** Last update dim. sept. 20 19:07:35 2015 Franck Hochstaetter
*/

#ifndef LEM_STRUCT_H_
# define LEM_STRUCT_H_

/*
** Graphic lem_in
*/
typedef enum		e_pos_cam
  {
			POS_X,
			POS_Y,
			POS_Z
  }			t_pos_cam;

typedef enum		e_vect_cam
  {
			POS,
			ROT
  }			t_vect_cam;

typedef struct		s_cam
{
  double		pos[3];
  double		target[3];
  double		vert[3];
  double		mv[2][3];
}			t_cam;

/*
** Parsing lem_in
*/
typedef	enum		e_connect
  {
			NO,
			YES
  }			t_connect;

typedef	struct		s_top
{
  char			*name;
  double		coord[3];
  t_connect		*connect;
  struct s_top		*next;
  struct s_top		**branch;
}			t_top;

typedef struct		s_ants
{
  double		pos[3];
  double		forward[3];
  unsigned int		turn;
  t_top			*now;
  t_top			*after;
  t_top			*path;
}			t_ants;

typedef	struct		s_ogl_graph
{
  unsigned int		nb_ants;
  unsigned int		turn;
  unsigned int		nb_start;
  t_top			*top;
  t_top			*start;
  t_top			*end;
  t_top			**path;
  t_ants		*ants;
}			t_ogl_graph;

#endif /* !LEM_STRUCT_H_ */
