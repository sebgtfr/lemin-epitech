/*
** parse_graphic.h for include in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/include
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Tue Sep 15 14:53:54 2015 sebastien le-maire
** Last update dim. sept. 20 19:10:44 2015 Franck Hochstaetter
*/

#ifndef PARSE_GRAPHIC_H_
# define PARSE_GRAPHIC_H_

# include "lem_struct.h"

/*
** MSG ERROR
*/
# define MSG_ERR_COORD_INVALID	"Error: Invalid coordinate!\n"
# define MSG_ERR_NAME_COORD	"Error: Malloc of name-coord failed!\n"
# define MSG_ERR_INIT_TOP	"Error: Initialisation of top failed!\n"
# define MSG_ERR_CMD_BAD_PLACE	"Error: Command badly placed!\n"
# define MSG_ERR_STX_UNKNOWN	"Error: Synthax unknown!\n"
# define MSG_ERR_PATH		"Error: Pathline invalid!\n"
# define MSG_ERR_NOT_AGREE_NAME	"Error: Hex doesn't accept node begining by P\n"
# define MSG_ERR_TOP_NO_EXIST	"Error: Unknown Name!\n"
# define MSG_ERR_PATH_NOT_EXIST	"Error: Path isn't init!\n"
# define MSG_ERR_PATH_NOT_FOUND	"Error: Ants doesn't have path!\n"
# define MSG_ERR_NB_ANTS	"Error: Number of ants must be positive!\n"

/*
** init_graph.c
*/
int		link_graph(t_top **top, const unsigned int size);
void		link_nodes(t_top **list, t_top **parser,
			   const unsigned int size);
unsigned int	cpt_link(t_top *list, const unsigned int size);
int		init_graph(t_ogl_graph *graph);

/*
** list.c
*/
int		add_new_elem(t_top **p, const char *name,
			     const int coord_x, const int coord_y);
void		delete_top(t_top **top);
void		delete_graph(t_ogl_graph *graph);
unsigned int	node_len(t_top *top);

/*
** nb_init_ants.c
*/
unsigned int	get_nb_ants(void);
int	        init_ants(t_ogl_graph *graph);
int		fill_ants(t_ogl_graph *graph, unsigned int y,
			  unsigned int turn);
/*
** get_graph.c
*/
int		get_graph(t_ogl_graph *graph);
void		place_start_end(t_ogl_graph *graph,
				const unsigned int start_end);
int		check_comment_start_end(char **line);

/*
** synthax.c
*/
unsigned int	synthax_line(char *line, const unsigned int start_end,
			     const unsigned int old_synthax);
int		check_valid_name(char first_char_name);
unsigned int	synthax_name_coord(const char *line);
unsigned int	synthax_link(const char *line);
unsigned int	synthax_path(const char *line);

/*
** comment.c
*/
unsigned int	comment(const char *line);
unsigned int	check_comment(const char *line);

/*
** check_if_top_exist
*/
int		check_if_top_exist(const char *name1, const char *name2,
				   t_top *top);
int		check_if_top_exist_path(const char *name, t_top *top);

/*
** get_data.c
*/
int		get_top(t_ogl_graph *graph, char *line);
int		get_link(t_ogl_graph *graph, char *line);
int		get_path(t_ogl_graph *graph, char *line);

/*
** ants_path.c
*/
unsigned int	elem_path(const char *line);
int		init_path(t_ogl_graph *graph);
int		add_path(t_top **ants, t_top *top);
int		search_path(t_top **tmp, char *path);
void		delete_path(t_top **path);

/*
** third_dimension.c
*/
int		place_third_dimension(t_top *top, t_top *start);
void		init_third_dimension_middle(t_ogl_graph *graph);
void		add_third_dimension(t_top **tmp, unsigned int y);
void		third_dimension(t_ogl_graph *graph);

#endif /* !PARSE_GRAPHIC_H_ */
