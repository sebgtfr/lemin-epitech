/*
** my_put_err.c for src in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/PSU_2014_my_select/lib/my/src
** 
** Made by Franck Hochstaetter
** Login   <hochst_a@epitech.net>
** 
** Started on  Tue May 19 10:30:16 2015 Franck Hochstaetter
** Last update Tue May 19 10:46:12 2015 Franck Hochstaetter
*/

#include <unistd.h>

int	my_put_err(char *str)
{
  int i;

  i = -1;
  while (str[++i])
    write(2, str + i, 1);
  return (0);
}
