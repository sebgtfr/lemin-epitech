/*
** my_strndup.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:32:54 2015 sebastien le-maire
** Last update Mon Aug 10 10:32:55 2015 sebastien le-maire
*/

#include <stdlib.h>
#include "myc.h"

char		*my_strndup(const char *src, const unsigned int size)
{
  char		*dest;

  if (!(dest = newstr(size)))
    return (NULL);
  return (my_strncpy(dest, src, size));
}
