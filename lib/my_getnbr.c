/*
** my_getnbr.c for lib in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/lib
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:31:17 2015 sebastien le-maire
** Last update dim. sept. 20 20:37:36 2015 Franck Hochstaetter
*/

#include "myc.h"

int		my_getnbr(const char *str, t_bool count_sign)
{
  int		nb;
  int		s;
  unsigned int	pos;
  unsigned int	cpt;

  pos = 0;
  cpt = 0;
  while (str[pos] && NOT_NUM(str[pos]))
    cpt += (str[pos++] == '-') ? 1 : 0;
  if (count_sign == TRUE)
    s = ((cpt % 2) == 1) ? -1 : 1;
  else
    s = (pos > 0 && str[pos - 1] == '-') ? -1 : 1;
  nb = 0;
  while (str[pos] && IS_NUM(str[pos]))
    {
      nb *= 10;
      nb += CHAR_TO_INT(str[pos]);
      ++pos;
    }
  return (nb * s);
}
