/*
** fail.c for lib in /home/dasfranck/osuce/hochst_a/Epitech/Tek1/CPE_2014_lemin/lib
** 
** Made by Franck Hochstaetter
** Login   <hochst_a@epitech.net>
** 
** Started on  Tue May 26 15:31:09 2015 Franck Hochstaetter
** Last update dim. sept. 20 14:17:22 2015 Franck Hochstaetter
*/

#include <stdlib.h>
#include <unistd.h>
#include "../include/myc.h"

void	fail(char *mesa, int err)
{
  if (err != 0)
    write(2, "FAIL: ", 6);
  my_put_err(mesa);
  write(2, "\n", 1);
  exit(err);
}
