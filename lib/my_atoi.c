/*
** my_atoi.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:31:04 2015 sebastien le-maire
** Last update Mon Sep 14 18:25:12 2015 sebastien le-maire
*/

#include "myc.h"

int		my_atoi(const char *str)
{
  int		nb;
  int		s;
  unsigned int	pos;

  if (!str[0] || (str[0] != '-' && NOT_NUM(str[0])))
    return (-1);
  s = (str[0] == '-') ? -1 : 1;
  pos = (s == -1) ? 1 : 0;
  nb = 0;
  while (IS_NUM(str[pos]))
    {
      nb *= 10;
      nb += CHAR_TO_INT(str[pos]);
      ++pos;
    }
  return (nb * s);
}
