/*
** my_puterror.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:30:29 2015 sebastien le-maire
** Last update Mon Aug 10 10:30:30 2015 sebastien le-maire
*/

#include <unistd.h>
#include "myc.h"

void	my_puterror(const char *str)
{
  (void)write(2, str, my_strlen(str));
}
