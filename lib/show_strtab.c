/*
** show_strtab.c for libc in /home/le-mai_s/librairie/librairie_C/my_libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Tue Aug 11 08:53:51 2015 sebastien le-maire
** Last update Tue Aug 11 09:08:39 2015 sebastien le-maire
*/

#include "myc.h"

void		show_strtab(const char **tab, const char *separator)
{
  unsigned int	y;

  y = 0;
  if (!tab)
    return ;
  while (tab[y])
    {
      if (separator && y > 0)
	my_putstr(separator);
      my_putstr(tab[y++]);
    }
  my_putchar('\n');
}
