/*
** get_next_line.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:33:58 2015 sebastien le-maire
** Last update Mon Aug 10 10:34:00 2015 sebastien le-maire
*/

#include <stdlib.h>
#include <unistd.h>
#include "myc.h"

void		gnl_free_save(char **save)
{
  free(*save);
  *save = NULL;
}

int		gnl_prep_line(t_gnl *p, char **save, int ret)
{
  int		i;
  unsigned int	size;

  i = -1;
  if (p->tmp)
    while (p->tmp[++i])
      {
	if (p->tmp[i] == '\n' || (!p->tmp[i + 1] && !ret))
	  {
	    size = my_strlen(p->tmp) - my_strlen(p->tmp + i);
	    if (!(p->line = newstr(size)))
	      return (2);
	    p->line = my_strncpy(p->line, p->tmp, size);
	    size = (my_strlen(p->tmp) - size);
	    if (!(*save = newstr(size)))
	      return (2);
	    *save = my_strncpy(*save, (p->tmp + (++i)), size);
	    if (!(*save[0]))
	      gnl_free_save(save);
	    free(p->tmp);
	    return (1);
	  }
      }
  return (0);
}

int		gnl_transfer_save(t_gnl *p, char **save)
{
  if (*save)
    {
      p->tmp = my_strncpy(p->tmp, *save, BUFF_SIZE);
      gnl_free_save(save);
      return (1);
    }
  return (0);
}

char		*gnl_realloc(char *s1, const char *s2)
{
  char		*tmp;

  if ((!s1[0] && !s2[0]))
    return (NULL);
  else if ((s1[0] && !s2[0]))
    return (s1);
  if ((!s1[0] && s2[0]))
    {
      if (!(tmp = my_strdup(s2)))
	return (NULL);
      free(s1);
      return (tmp);
    }
  if (!(tmp = newstr(my_strlen(s1) + my_strlen(s2) + 1)))
    return (NULL);
  tmp = my_strcpy(tmp, s1);
  tmp = my_strcat(tmp, s2);
  free(s1);
  return (tmp);
}

char		*get_next_line(const int fd)
{
  static char	*save = NULL;
  static int	ret = 1;
  t_gnl		p;

  if (!(p.tmp = newstr(BUFF_SIZE)))
    return (NULL);
  my_memset((void *)p.buf, BUFF_SIZE + 1);
  p.line = NULL;
  if (gnl_transfer_save(&p, &save))
    if (gnl_prep_line(&p, &save, ret))
      return (p.line);
  while ((ret = read(fd, p.buf, BUFF_SIZE)) > 0)
    {
      if (!(p.tmp = gnl_realloc(p.tmp, p.buf)))
	return (NULL);
      if (gnl_prep_line(&p, &save, ret))
	return (p.line);
      my_memset((void *)p.buf, BUFF_SIZE);
    }
  if (p.tmp && p.tmp[0])
    return (p.tmp);
  free(p.tmp);
  return (NULL);
}
