/*
** my_putnbr_base.c for libc in /home/le-mai_s/librairie/librairie_C/my_libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Tue Aug 11 09:27:30 2015 sebastien le-maire
** Last update Tue Aug 11 09:43:30 2015 sebastien le-maire
*/

#include "myc.h"

void		my_putnbr_base(int nbr, const char *base)
{
  unsigned int	size_base;
  unsigned int	divisor;
  unsigned int	nb;

  divisor = 1;
  size_base = my_strlen(base);
  if (nbr < 0)
    {
      my_putchar('-');
      nbr *= -1;
    }
  while ((nbr / divisor) >= size_base)
    divisor *= size_base;
  while (divisor >= size_base)
    {
      nb = (nbr - (nbr % divisor)) / divisor;
      my_putchar(base[nb]);
      nbr %= divisor;
      divisor /= size_base;
    }
  my_putchar(base[nbr]);
}
