/*
** my_strcpy.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:31:32 2015 sebastien le-maire
** Last update Mon Aug 10 10:31:33 2015 sebastien le-maire
*/

#include "myc.h"

char		*my_strcpy(char *dest, const char *src)
{
  unsigned int	i;

  i = 0;
  while (src[i])
    {
      dest[i] = src[i];
      ++i;
    }
  dest[i] = '\0';
  return (dest);
}
