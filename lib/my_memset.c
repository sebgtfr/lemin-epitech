/*
** my_memset.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:33:48 2015 sebastien le-maire
** Last update Mon Aug 10 10:33:49 2015 sebastien le-maire
*/

#include "myc.h"

void		*my_memset(void *mem, const unsigned int size)
{
  unsigned int	i;

  i = 0;
  while (i < size)
    ((char *)mem)[i++] = '\0';
  return (mem);
}
